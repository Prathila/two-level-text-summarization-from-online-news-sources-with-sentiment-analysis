from django.contrib import admin
from.models import user,news,newssummary

# Register your models here.



class useradmin(admin.ModelAdmin):
    list_display=('username','email')
    search_fields=('username','email')

admin.site.register(user,useradmin)
class newsadmin(admin.ModelAdmin):
    list_display=('userid','news')
    search_fields=('userid','news')

admin.site.register(news,newsadmin)
class newssummaryadmin(admin.ModelAdmin):
    list_display=('userid','newsid','summary')
    search_fields=('userid','newsid','summary')
    
admin.site.register(newssummary,newssummaryadmin)
