from django.db import models

# Create your models here.

class user(models.Model):
    username=models.CharField(max_length=50)
    email=models.CharField(max_length=50)
    password=models.CharField(max_length=10)
    address=models.CharField(max_length=50)
    phone=models.CharField(max_length=15)
    status=models.CharField(max_length=15)
    usertype=models.CharField(max_length=15)
    def __str___(self):
        return self.username

class news(models.Model):
    userid=models.ForeignKey(user,on_delete=models.CASCADE)
    news=models.CharField(max_length=250)
    def __str__(self):
        return self.userid.username

class newssummary(models.Model):
    newsid=models.ForeignKey(news,on_delete=models.CASCADE)
    userid=models.ForeignKey(user,on_delete=models.CASCADE)
    summary=models.CharField(max_length=150)
    rating=models.CharField(max_length=15)
    def __str__(self):
        return self.newsid.news