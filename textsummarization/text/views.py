from django.shortcuts import render
from django.http import HttpResponse
from .models import user

# Create your views here.

def home(request):
    my_dict={
        'A':"Summarising",
        'B':"Text"
    }
    return render(request,'index.html',context=my_dict)

def dataDisplay(request):
    text_list=user.objects.all()
    mydict={
        'data':text_list
    }
    return render(request,'user.html',context=mydict)

   