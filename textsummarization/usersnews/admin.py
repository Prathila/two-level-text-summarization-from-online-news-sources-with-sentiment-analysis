from django.contrib import admin
from .models import userTypes,MainNews

# Register your models here.

admin.site.site_header="News"
admin.site.site_title="News"
admin.site.index_title="News"

class MainNewsAdmin(admin.ModelAdmin):
    list_display=('mainnews',"summary")
    search_fields=('mainnews',)

class userTypesAdmin(admin.ModelAdmin):
    list_display=('name','Address')
    search_fields=('name',)



admin.site.register(MainNews,MainNewsAdmin)
admin.site.register(userTypes,userTypesAdmin)
