# Generated by Django 2.1.7 on 2019-04-26 06:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usersnews', '0004_usertypes_sentiments'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usertypes',
            name='sentiments',
        ),
        migrations.AddField(
            model_name='mainnews',
            name='sentiments',
            field=models.CharField(default=0, max_length=40),
            preserve_default=False,
        ),
    ]
