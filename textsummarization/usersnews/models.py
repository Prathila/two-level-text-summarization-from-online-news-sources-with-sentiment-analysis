from django.db import models

# Create your models here.

class userTypes(models.Model):
    userStatus= (
    ('A', 'Approve'),
    ('R', 'Reject'),
    ('NA', 'Not Approved'),
    
    )
    name=models.CharField(max_length=50)
    Address=models.CharField(max_length=100)
    UserName=models.CharField(max_length=40)
    Password=models.CharField(max_length=40)
    userTypes=models.CharField(max_length=40)
    block=models.CharField(max_length=20,choices=userStatus, unique=False)
    
    def __str__(self):
       return self.name

class MainNews(models.Model):
    Rate= (
    ('1', '*'),
    ('2', '**'),
    ('3', '***'),
    ('4', '****'),
    ('5', '*****'),
    
    )
    UserName=models.ForeignKey(userTypes,on_delete=models.CASCADE)
    mainnews=models.TextField(blank=True,)
    summary=models.CharField(max_length=400)
    Rating=models.CharField(max_length=20,choices=Rate, unique=False)
    sentiments=models.CharField(max_length=40)
    def __str__(self):
       return self.mainnews



